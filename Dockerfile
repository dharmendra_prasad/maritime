FROM node:8
RUN npm install -g @angular/cli --unsafe

# Create app directory
WORKDIR /app

ADD . /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json /app/

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

EXPOSE 9000

ENTRYPOINT [ "npm", "start", "--disable-host-check" ]



# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/  
