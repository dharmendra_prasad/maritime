import { NgModule } from '@angular/core';

import { MaritimefrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [MaritimefrontendSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [MaritimefrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class MaritimefrontendSharedCommonModule {}
