import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-carousel',
  templateUrl: './carousel.component.html',
  styles: ['carousel.css'],
  providers: [NgbCarouselConfig]
})
export class CarouselComponent implements OnInit {

  images: string[] = [];
  constructor(config: NgbCarouselConfig) {
    config.interval = 4000;
    config.wrap = true;
    config.keyboard = false;
  }

  ngOnInit() {
    this.images.push('../../../content/images/home_page_carousel/image1.jpg');
    this.images.push('../../../content/images/home_page_carousel/image2.jpg');
    this.images.push('../../../content/images/home_page_carousel/image5.jpg');
    this.images.push('../../../content/images/home_page_carousel/image6.jpg');
    this.images.push('../../../content/images/home_page_carousel/image7.jpg');
  }

}
