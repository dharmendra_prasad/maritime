import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaritimefrontendSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { CarouselComponent } from 'app/home/carousel/carousel.component';

@NgModule({
    imports: [MaritimefrontendSharedModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent, CarouselComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MaritimefrontendHomeModule {}
