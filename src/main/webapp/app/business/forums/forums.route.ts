import { Route } from '@angular/router';

import { ForumsComponent } from 'app/business/forums/forums.component';

export const forumsRoute: Route = {
    path: 'forums',
    component: ForumsComponent,
    data: {
        pageTitle: 'Forums'
    }
};
