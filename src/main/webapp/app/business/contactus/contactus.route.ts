import { Route } from '@angular/router';

import { ContactusComponent } from 'app/business/contactus/contactus.component';

export const contactusRoute: Route = {
    path: 'contactus',
    component: ContactusComponent,
    data: {
        pageTitle: 'Contact Us'
    }
};
