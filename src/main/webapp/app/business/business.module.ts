import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesComponent } from './services/services.component';
import { businessState } from 'app/business/business.route';
import { RouterModule } from '@angular/router';
import { ForumsComponent } from './forums/forums.component';
import { BlogsComponent } from './blogs/blogs.component';
import { EbooksComponent } from './ebooks/ebooks.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { SinglePageComponent } from './blogs/single-page/single-page.component';
import { SummaryComponent } from './blogs/summary/summary.component';
import { MobileappComponent } from './mobileapp/mobileapp.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(businessState)
  ],
  declarations: [
    ServicesComponent,
    ForumsComponent,
    BlogsComponent,
    EbooksComponent,
    AboutusComponent,
    ContactusComponent,
    SinglePageComponent,
    SummaryComponent,
    MobileappComponent],
  entryComponents: [ServicesComponent]
})
export class BusinessModule { }
