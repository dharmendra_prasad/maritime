import { Route } from '@angular/router';

import { EbooksComponent } from 'app/business/ebooks/ebooks.component';

export const ebooksRoute: Route = {
    path: 'ebooks',
    component: EbooksComponent,
    data: {
        pageTitle: 'e Books'
    }
};
