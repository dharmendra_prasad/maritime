import { Route } from '@angular/router';
import { MobileappComponent } from 'app/business/mobileapp/mobileapp.component';

export const mobileAppRoute: Route = {
    path: 'app',
    component: MobileappComponent,
    data: {
        pageTitle: 'Download App'
    }
};
