import { Route } from '@angular/router';

import { BlogsComponent } from 'app/business/blogs/blogs.component';

export const blogsRoute: Route = {
    path: 'blogs',
    component: BlogsComponent,
    data: {
        pageTitle: 'Blogs'
    }
};
