import { Route } from '@angular/router';

import { ServicesComponent } from 'app/business/services/services.component';

export const servicesRoute: Route = {
    path: 'services',
    component: ServicesComponent,
    data: {
        pageTitle: 'Services'
    }
};
