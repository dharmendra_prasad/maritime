import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { servicesRoute } from 'app/business/services/services.route';
import { forumsRoute } from 'app/business/forums/forums.route';
import { ebooksRoute } from 'app/business/ebooks/ebooks.route';
import { aboutusRoute } from 'app/business/aboutus/aboutus.route';
import { blogsRoute } from 'app/business/blogs/blogs.route';
import { contactusRoute } from 'app/business/contactus/contactus.route';
import { mobileAppRoute } from 'app/business/mobileapp/mobileapp.route';

const BUSINESS_ROUTES = [servicesRoute, forumsRoute, ebooksRoute, blogsRoute, aboutusRoute, contactusRoute, mobileAppRoute];

export const businessState: Routes = [
  {
      path: '',
      children: BUSINESS_ROUTES
  }
];
