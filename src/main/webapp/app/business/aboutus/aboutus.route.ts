import { Route } from '@angular/router';

import { AboutusComponent } from 'app/business/aboutus/aboutus.component';

export const aboutusRoute: Route = {
    path: 'aboutus',
    component: AboutusComponent,
    data: {
        pageTitle: 'About Us'
    }
};
